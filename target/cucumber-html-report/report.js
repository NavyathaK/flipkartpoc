$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/features/LogintoApplication.feature");
formatter.feature({
  "line": 1,
  "name": "Add a product to cart",
  "description": "",
  "id": "add-a-product-to-cart",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 6,
  "name": "Search and Add iPhoneX to the cart",
  "description": "",
  "id": "add-a-product-to-cart;search-and-add-iphonex-to-the-cart",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "submit valid Email/MobileNo as \"\u003cMobileNo\u003e\" and password as \"\u003cPassword\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "user is able to see home page",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "user is on Flipkart HomePage",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "user search for product as \"\u003cProduct\u003e\"and Add to cart",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "product \"\u003cProduct\u003e\" is displayed in Cart page",
  "keyword": "Then "
});
formatter.examples({
  "line": 14,
  "name": "",
  "description": "",
  "id": "add-a-product-to-cart;search-and-add-iphonex-to-the-cart;",
  "rows": [
    {
      "cells": [
        "MobileNo",
        "Password",
        "Product"
      ],
      "line": 15,
      "id": "add-a-product-to-cart;search-and-add-iphonex-to-the-cart;;1"
    },
    {
      "cells": [
        "9959827791",
        "Planit@123",
        "iPhone X"
      ],
      "line": 16,
      "id": "add-a-product-to-cart;search-and-add-iphonex-to-the-cart;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "user navigate to the login page",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginToApplication.user_navigate_to_the_login_page()"
});
formatter.result({
  "duration": 30537040177,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "Search and Add iPhoneX to the cart",
  "description": "",
  "id": "add-a-product-to-cart;search-and-add-iphonex-to-the-cart;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "submit valid Email/MobileNo as \"9959827791\" and password as \"Planit@123\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "user is able to see home page",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "user is on Flipkart HomePage",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "user search for product as \"iPhone X\"and Add to cart",
  "matchedColumns": [
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "product \"iPhone X\" is displayed in Cart page",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "9959827791",
      "offset": 32
    },
    {
      "val": "Planit@123",
      "offset": 61
    }
  ],
  "location": "LoginToApplication.submit_valid_username_as_and_password_as(String,String)"
});
formatter.result({
  "duration": 25745771,
  "status": "passed"
});
formatter.match({
  "location": "LoginToApplication.user_is_able_to_see_home_page()"
});
formatter.result({
  "duration": 64944,
  "status": "passed"
});
formatter.match({
  "location": "LoginToApplication.user_is_on_Flipkart_HomePage()"
});
formatter.result({
  "duration": 6831116112,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "iPhone X",
      "offset": 28
    }
  ],
  "location": "LoginToApplication.user_search_for_product_as_and_Add_to_cart(String)"
});
formatter.result({
  "duration": 32482266320,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "iPhone X",
      "offset": 9
    }
  ],
  "location": "LoginToApplication.product_is_displayed_in_Cart_page(String)"
});
formatter.result({
  "duration": 7847703644,
  "status": "passed"
});
formatter.after({
  "duration": 4635562134,
  "status": "passed"
});
});
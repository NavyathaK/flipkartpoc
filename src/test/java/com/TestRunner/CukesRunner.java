package com.TestRunner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
	
	//@RunWith(Cucumber.class) 
	@CucumberOptions(	
			
	plugin= {"html:target/cucumber-html-report",	
			 "json:target/cucumber.json",			
	         "pretty:target/cucumber-pretty.txt"},
			
			
	features= {"src/test/java/features/LogintoApplication.feature"},
	glue= {"stepDefination"}		
			
	)
	
	public class CukesRunner extends AbstractTestNGCucumberTests{
} 

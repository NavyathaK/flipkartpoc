package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.AssertJUnit;

import io.appium.java_client.android.AndroidDriver;

public class HomePage extends BasePage{
	public String   fieldName;
	public HomePage(Object mDriver) {
		super(mDriver);

	}

	public void clickButton(String field){


		if(field.contains("menu")){
			fieldName =prop.getProperty("Menu");
		}
		else if(field.contains("signIn")){
			fieldName =prop.getProperty("Signin");
		}
		else if(field.contains("home")){
			fieldName =prop.getProperty("Home");
		}
		else if(field.contains("search")){
			fieldName =prop.getProperty("Search");
		}


		bStatus = click("xpath",fieldName,60);

		Assert.assertEquals(bStatus, true, "Element is not Clickable");

	}
	
	public void enterText(String Product){
		
		

		bStatus = sendText("id", prop.getProperty("SearchText"),Product,60);

		Assert.assertEquals(bStatus,true,"Product not entered");
		
		bStatus = click("xpath", "//android.widget.TextView[@text='iphone x']/following-sibling::android.widget.TextView[@text='in Mobiles']",60);

		Assert.assertEquals(bStatus,true,"Product not entered");	
		
//		clickEnter();
//		clickEnter();
//		clickEnter();
//		clickEnter();
	}
	

	public void clickingLinks(String field){		


		if(field.contains("myAccount")){
			fieldName =prop.getProperty("MyAccount");
		}
		else if(field.contains("Show")){
			fieldName =prop.getProperty("WebShow");
		}
		else if(field.contains("Savedetails")){
			fieldName =prop.getProperty("savedetails");
		}
		else if(field.contains("FqaSelect")){
			fieldName =prop.getProperty("fqaSlect");
		}


		bStatus = click("xpath",fieldName,60);

		Assert.assertEquals(bStatus, true, "Element is not Clickable");
	}

	

	public void elementVisiblity(String field){

		if(field.equalsIgnoreCase("recyclerView"))
			fieldName =prop.getProperty("RecyclerView");

		else if(field.contains("ErrorEmail")){
			fieldName =prop.getProperty("EmailErroeweb");
		}
		else if(field.contains("Inavalidcredentials")){
			fieldName =prop.getProperty("Inavlidcredentials");
		}
		else if(field.contains("Accountdetail")){
			fieldName =prop.getProperty("AccountDetails");
		}
		
		bStatus = elementVisiblity("xpath",fieldName,60);

		Assert.assertEquals(bStatus, true, "Element is not Clickable");
	}
	
	 public void selectingProduct(){
		 

		 
			bStatus = clickDynamic("xpath", prop.getProperty("Product"),"Apple iPhone X (Space Gray, 64 GB)", 60);
			if (!bStatus) {
				System.out.println("Element is not clickable");
				AssertJUnit.fail("Element is not clickable");
			}
		
	 }
	 
	 public void clickingSearch(){
			bStatus = click("id", prop.getProperty("Search"), 60);
			if (!bStatus) {
				System.out.println("Element is not clickable");
				AssertJUnit.fail("Element is not clickable");
			}
		
	 }
}

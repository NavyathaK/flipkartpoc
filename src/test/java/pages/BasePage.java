package pages;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.ios.IOSDriver;
import setup.AndroidSetup;

import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;



public class BasePage extends AndroidSetup{

	//	public static  AndroidDriver mDriver;
	public static File driverDir;

	public static WebDriverWait wait;
	public static boolean bStatus;

	public BasePage(Object mDriver) {
		super();

	}

	public static boolean sendText(String typeClick, String elementClick, String inputText, int timeOut)
	{
		WebElement x = FindElement(typeClick, elementClick, timeOut);
		try
		{
			x.clear();
			x.click();
			x.sendKeys(inputText);
			return true;
		} catch (Exception e)
		{
			System.out.println("Adding text to text field / area has failed: " + e.getMessage());
			return false;
		}
	}
	public static boolean pullDown() {
		try{
			if(property.get("platform-name").equals("Android")){
				int screenWidth = ((AndroidDriver)mDriver).manage().window().getSize().getWidth();
				int screenHeight = ((AndroidDriver)mDriver).manage().window().getSize().getHeight();

				int startX = screenWidth / 2;
				int endX = screenWidth / 2;
				int startY = screenHeight*4/6;
				int endY = screenHeight*5/6;

				((AndroidDriver)mDriver).swipe(startX, startY, endX, endY, 2000);
				return true;
			}
			else{
				int screenWidth = ((IOSDriver)mDriver).manage().window().getSize().getWidth();
				int screenHeight = ((IOSDriver)mDriver).manage().window().getSize().getHeight();

				int startX = screenWidth / 2;
				int endX = screenWidth / 2;
				int startY = screenHeight*4/6;
				int endY = screenHeight*5/6;

				((IOSDriver)mDriver).swipe(startX, startY, endX, endY, 2000);
				return true;
			}      
		}
		catch(Exception e){
			return false;
		}

	}

	public static void switchToSettingsApp(){

		((AndroidDriver) mDriver).startActivity("com.android.settings","com.android.settings.Settings");

	}

	public static void switchToCostaApp(){

		((AndroidDriver) mDriver).startActivity("uk.co.club.costa.costa.dev","uk.co.club.costa.costa.features.home.HomeActivity");

	}

	public static void tap(){
		TouchAction action = new TouchAction((AndroidDriver) mDriver);
		if(!property.get("deviceName").equals("OPPO F1s")){	
			action.press(826, 417).release().perform();

		}
		else{
			action.press(544,250).release().perform();
		}
		System.out.println("Done");
	}

	public static void tapElement(){
		TouchAction action = new TouchAction((AndroidDriver) mDriver);
		if(!property.get("deviceName").equals("OPPO F1s")){	
			action.press(958, 576).release().perform();
		}
		else{
			action.press(348, 532).release().perform();
		}
		System.out.println("Done");
	}

	//	public static void tap() throws InterruptedException{
	//
	//		HashMap<String, Double> point = new HashMap<String, Double>();
	//		point.put("x", 826.0d);
	//		point.put("y", 417.0d);
	//		JavascriptExecutor jse = (JavascriptExecutor)((AndroidDriver) mDriver);
	//		jse.executeScript("mobile: tap", point);
	//		Thread.sleep(2000);
	//
	//		System.out.println("Done");
	//	}

	//	public static void tap() throws InterruptedException{
	//
	//		((AndroidDriver) mDriver).tap(1, 826, 417, 1000);
	//
	//		System.out.println("Done");
	//	}



	public static String getTitle(){

		try
		{
			Thread.sleep(10000);
			String title = ((AndroidDriver)mDriver).getTitle();
			System.out.println(title);
			return title;
		} catch (Exception e)
		{
			System.out.println("Adding text to text field / area has failed: " + e.getMessage());
			return null;
		}
	}

	public static boolean click(String typeClick, String elementClick, int timeOut)
	{
		WebElement x = FindElement(typeClick, elementClick, timeOut);
		try
		{	
			x.click();

			System.out.println("Found " + typeClick + " element " + elementClick);
			return true;
		} catch (Exception e)
		{
			System.out.println("Unable to find " + typeClick + " element " + elementClick);
			return false;
		}
	}
	
	public static String getText(String typeClick, String elementClick, int timeOut)
	{
		WebElement x = FindElement(typeClick, elementClick, timeOut);
		try
		{
			return x.getText();

		} catch (Exception e)
		{
			System.out.println(x);
			System.out.println("Unable to find text " + typeClick + " element " + elementClick);
			return "DID NOT FIND TEXT WITHIN ELEMENT";
		}
	}

	public static boolean elementVisiblity(String typeClick, String elementClick, int timeOut)
	{
		WebElement x = FindElement(typeClick, elementClick, timeOut);
		try
		{	
			x.isDisplayed();

			System.out.println("Found " + typeClick + " element " + elementClick);
			return true;
		} catch (Exception e)
		{
			System.out.println("Unable to find " + typeClick + " element " + elementClick);
			return false;
		}
	} 
	
	public static boolean elementVisiblityDynamic(String typeClick, String element, String value,int timeOut){


		String elementVisible =element.replace("dynamic",value);

		WebElement x = FindElement(typeClick, elementVisible, timeOut);

		try
		{	

			System.out.println("Found element " + elementVisible);
			return true;
		} catch (Exception e)
		{
			System.out.println("Unable to find element " + elementVisible);
			return false;
		}

	}
	public static boolean clickEnter() {

		try{
	//		((AndroidDriver) mDriver).pressKeyCode(AndroidKeyCode.ENTER);
			
			((AndroidDriver) mDriver).pressKeyCode(160);
			
		//	((AndroidDriver) mDriver).executeScript("mobile:keyevent", "keycode:66");
			
			((AndroidDriver) mDriver).getKeyboard().sendKeys(Keys.ENTER);

			return true;
		}
		catch(Exception e){
			return false;
		}
	}
	
	public static boolean setKeyboard() {

		try{
			((AndroidDriver) mDriver).pressKeyCode(AndroidKeyCode.ENTER);

			return true;
		}
		catch(Exception e){
			return false;
		}
	}
	
	
	
	public static void navBack() throws InterruptedException{
		if(property.get("platform-name").equals("Android")){
			Thread.sleep(2000);

			((AndroidDriver) mDriver).navigate().back();
		}
		else{

			Thread.sleep(2000);
			((IOSDriver) mDriver).navigate().back();

		}
	} 

		public static void getKeyboard(){
			if(property.get("platform-name").equals("Android"))
	
				((AndroidDriver) mDriver).getKeyboard();
	
			else
				((IOSDriver) mDriver).hideKeyboard();
	
		} 

	public static WebElement FindElement(String type, String element, int timeOut)
	{
		type = type.toLowerCase();
		WebElement x = null;
		try
		{
			switch (type)
			{
			case "id":
				if(property.get("platform-name").equals("Android"))
					x = new WebDriverWait((AndroidDriver)mDriver, timeOut).until(ExpectedConditions.presenceOfElementLocated(By.id(element)));
				else
					x = new WebDriverWait((IOSDriver)mDriver, timeOut).until(ExpectedConditions.presenceOfElementLocated(By.id(element)));
				break;
			case "xpath":
				if(property.get("platform-name").equals("Android"))
					x = new WebDriverWait((AndroidDriver)mDriver, timeOut).until(ExpectedConditions.presenceOfElementLocated(By.xpath(element)));
				else
					x = new WebDriverWait((IOSDriver)mDriver, timeOut).until(ExpectedConditions.presenceOfElementLocated(By.xpath(element)));
				break;
			case "class":
				if(property.get("platform-name").equals("Android"))
					x = new WebDriverWait((AndroidDriver)mDriver, timeOut).until(ExpectedConditions.presenceOfElementLocated(By.className(element)));
				else
					x = new WebDriverWait((IOSDriver)mDriver, timeOut).until(ExpectedConditions.presenceOfElementLocated(By.className(element)));	
				break;
			case "link":
				if(property.get("platform-name").equals("Android"))
					x = new WebDriverWait((AndroidDriver)mDriver, timeOut).until(ExpectedConditions.presenceOfElementLocated(By.linkText(element)));
				else
					x = new WebDriverWait((IOSDriver)mDriver, timeOut).until(ExpectedConditions.presenceOfElementLocated(By.linkText(element)));
				break;
			case "css":
				if(property.get("platform-name").equals("Android"))
					x = new WebDriverWait((AndroidDriver)mDriver, timeOut).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(element)));
				else
					x = new WebDriverWait((IOSDriver)mDriver, timeOut).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(element)));	
				break;
			case "name":
				if(property.get("platform-name").equals("Android"))
					x = new WebDriverWait((AndroidDriver)mDriver, timeOut).until(ExpectedConditions.presenceOfElementLocated(By.name(element)));
				else
					x = new WebDriverWait((IOSDriver)mDriver, timeOut).until(ExpectedConditions.presenceOfElementLocated(By.name(element)));		
				break;
			case "tag":
				if(property.get("platform-name").equals("Android"))
					x = new WebDriverWait((AndroidDriver)mDriver, timeOut).until(ExpectedConditions.presenceOfElementLocated(By.tagName(element)));
				else
					x = new WebDriverWait((IOSDriver)mDriver, timeOut).until(ExpectedConditions.presenceOfElementLocated(By.tagName(element)));	
				break;

			default:
				System.out.println("***The type to find before does not exist***");
				break;
			}
			return x;
		} catch (Exception e)
		{
			System.out.println("Unable to find " + type + " element " + element);
			return null;
		}

	}

	//	public static boolean clickButton(By locator) {
	//		try {
	//			if(prop.get("platform-name").equals("Android")){
	//			System.out.println("Driver value ===== " +mDriver);
	//			wait = new WebDriverWait(((AndroidDriver)mDriver), 20);
	//			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	//
	//			bStatus = ((AndroidDriver)mDriver).findElement(locator).isEnabled();
	//			if (bStatus) {
	//
	//				((AndroidDriver)mDriver).findElement(locator).click();
	//
	//				return true;
	//			}
	//			}
	//			else{
	//				
	//			}
	//
	//		} catch (Exception e) {
	//
	//			e.printStackTrace();
	//			return false;
	//		}
	//
	//		return bStatus;
	//
	//	}
	//
	//
	//	public static void swipeRight(AndroidDriver mDriver, int screenWidth, int screenHeight, int q, int o) {
	//
	//
	//		int startX = screenWidth * q / 9;
	//		int endX = screenWidth * o / 9;
	//		int startY = screenHeight / 2;
	//		int endY = screenHeight / 2;
	//
	//
	//		mDriver.swipe(startX, startY, endX, endY, 2000);
	//
	//
	//
	//	}
	//
	//	public static void swipeleft(AndroidDriver mDriver, int screenWidth, int screenHeight, int s, int d) {
	//		;
	//
	//		int startY = screenHeight / 2;
	//		int endY = screenHeight / 2;
	//		int startX1 = screenWidth * s / 9;
	//		int endX1 = screenWidth * d / 9;
	//
	//		mDriver.swipe(startX1, startY, endX1, endY, 2000);
	//
	//	}
	//
	//	public static void swipeDown(AndroidDriver mDriver, int screenWidth, int screenHeight, int r, int t) {
	//
	//		int startX2 = screenWidth / 2;
	//		int endX2 = screenWidth / 2;
	//		int startY1 = screenHeight * r / 9;
	//		int endY1 = screenHeight * t / 9;
	//
	//		mDriver.swipe(startX2, startY1, endX2, endY1, 2000);
	//
	//	}

	public static void swipeUP(AndroidDriver mDriver, int screenWidth, int screenHeight, int h, int f) {


		int startX2 = screenWidth / 2;
		int endX2 = screenWidth / 2;
		int startY2 = screenHeight * h / 9;
		int endY2 = screenHeight * f / 9;

		mDriver.swipe(startX2, startY2, endX2, endY2, 2000);

	}
	public static boolean ScrollUP() {
		try{
			if(property.get("platform-name").equals("Android")){
				int screenWidth = ((AndroidDriver)mDriver).manage().window().getSize().getWidth();
				int screenHeight = ((AndroidDriver)mDriver).manage().window().getSize().getHeight();

				int startX = screenWidth / 2;
				int endX = screenWidth / 2;
				int startY = screenHeight*5/6;
				int endY = screenHeight*2/6;

				((AndroidDriver)mDriver).swipe(startX, startY, endX, endY, 2000);
				return true;
			}
			else{
				int screenWidth = ((IOSDriver)mDriver).manage().window().getSize().getWidth();
				int screenHeight = ((IOSDriver)mDriver).manage().window().getSize().getHeight();

				int startX = screenWidth / 2;
				int endX = screenWidth / 2;
				int startY = screenHeight*5/6;
				int endY = screenHeight*2/6;

				((IOSDriver)mDriver).swipe(startX, startY, endX, endY, 2000);
				return true;
			}      
		}
		catch(Exception e){
			return false;
		}



	}
	public static boolean pullRefresh() {
		try{
			if(property.get("platform-name").equals("Android")){
				int screenWidth = ((AndroidDriver)mDriver).manage().window().getSize().getWidth();
				int screenHeight = ((AndroidDriver)mDriver).manage().window().getSize().getHeight();

				int startX = screenWidth / 2;
				int endX = screenWidth / 2;
				int startY = screenHeight*4/6;
				int endY = screenHeight*5/6;

				((AndroidDriver)mDriver).swipe(startX, startY, endX, endY, 2000);
				return true;
			}
			else{
				int screenWidth = ((IOSDriver)mDriver).manage().window().getSize().getWidth();
				int screenHeight = ((IOSDriver)mDriver).manage().window().getSize().getHeight();

				int startX = screenWidth / 2;
				int endX = screenWidth / 2;
				int startY = screenHeight*4/6;
				int endY = screenHeight*5/6;

				((IOSDriver)mDriver).swipe(startX, startY, endX, endY, 2000);
				return true;
			}	
		}
		catch(Exception e){
			return false;
		}

	}
	public static boolean SwipeUP() {
		try{
			if(property.get("platform-name").equals("Android")){
			
				
				int screenWidth = ((AndroidDriver)mDriver).manage().window().getSize().getWidth();				
				int screenHeight = ((AndroidDriver)mDriver).manage().window().getSize().getHeight();


				int startX = screenWidth / 2;
				int endX = screenWidth / 2;
				int startY = screenHeight*5/6;
				int endY = screenHeight*2/6;
				
				
				

				((AndroidDriver)mDriver).swipe(startX, startY, endX, endY, 2000);
			
				return true;
			}
			else{
				int screenWidth = ((IOSDriver)mDriver).manage().window().getSize().getWidth();
				int screenHeight = ((IOSDriver)mDriver).manage().window().getSize().getHeight();

				int startX = screenWidth / 2;
				int endX = screenWidth / 2;
				int startY = screenHeight*5/6;
				int endY = screenHeight*2/6;
			
				((IOSDriver)mDriver).swipe(startX, startY, endX, endY, 2000);
				return true;
			}      
		}
		catch(Exception e){
			return false;
		}

	}

	public static boolean SwipeLeft() {
		try{
			if(property.get("platform-name").equals("Android")){
				int screenWidth = ((AndroidDriver)mDriver).manage().window().getSize().getWidth();
				int screenHeight = ((AndroidDriver)mDriver).manage().window().getSize().getHeight();

				int startX = screenWidth *2/6;
				int endX = screenWidth *4/6;
				int startY = screenHeight/2;
				int endY = screenHeight/2;

				((AndroidDriver)mDriver).swipe(startX, startY, endX, endY, 2000);
				return true;
			}
			else{
				int screenWidth = ((IOSDriver)mDriver).manage().window().getSize().getWidth();
				int screenHeight = ((IOSDriver)mDriver).manage().window().getSize().getHeight();

				int startX = screenWidth *5/ 6;
				int endX = screenWidth *2/ 6;
				int startY = screenHeight/2;
				int endY = screenHeight/2;

				((IOSDriver)mDriver).swipe(startX, startY, endX, endY, 2000);
				return true;
			}      
		}
		catch(Exception e){
			return false;
		}

	}

	public String takeScreenShot() {
		File scrFile = ((TakesScreenshot) mDriver).getScreenshotAs(OutputType.FILE); 

		driverDir = new File(System.getProperty("user.dir"));

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
		
		String destFile = dateFormat.format(new Date()) + ".png"; 
		
		try {
			FileUtils.copyFile(scrFile, new File(driverDir+"\\Screenshots\\"+ destFile)); 

		} catch (IOException e) {
			System.out.println("Image not transfered to screenshot folder");
			e.printStackTrace();
		}
		return destFile;
	}
	
	

	public static void clear(String typeClick, String elementClick,int timeOut)
	{
		WebElement x = FindElement(typeClick, elementClick, timeOut);
		try
		{
			x.clear();

		} catch (Exception e)
		{
			System.out.println("Adding text to text field / area has failed: " + e.getMessage());
		}
	}

	public static boolean getUrl(String Url )
	{
		((AndroidDriver) mDriver).get(Url);

		return true;

	}
	public static boolean clickDynamic(String typeClick, String element, String value,int timeOut){


		String elementClick = element.replace("dynamic",value);

		WebElement x = FindElement(typeClick, elementClick, timeOut);

		try
		{	
			x.click();

			System.out.println("Found " + typeClick + " element " + elementClick);
			return true;
		} catch (Exception e)
		{
			System.out.println("Unable to find " + typeClick + " element " + elementClick);
			return false;
		}

	}


	//	public static boolean clickButton(By locator) {
	//		try {
	//			if(prop.get("platform-name").equals("Android")){
	//			System.out.println("Driver value ===== " +mDriver);
	//			wait = new WebDriverWait(((AndroidDriver)mDriver), 20);
	//			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	//
	//			bStatus = ((AndroidDriver)mDriver).findElement(locator).isEnabled();
	//			if (bStatus) {
	//
	//				((AndroidDriver)mDriver).findElement(locator).click();
	//
	//				return true;
	//			}
	//			}
	//			else{
	//				
	//			}
	//
	//		} catch (Exception e) {
	//
	//			e.printStackTrace();
	//			return false;
	//		}
	//
	//		return bStatus;
	//
	//	}
	//
	//
	//	public static void swipeRight(AndroidDriver mDriver, int screenWidth, int screenHeight, int q, int o) {
	//
	//
	//		int startX = screenWidth * q / 9;
	//		int endX = screenWidth * o / 9;
	//		int startY = screenHeight / 2;
	//		int endY = screenHeight / 2;
	//
	//
	//		mDriver.swipe(startX, startY, endX, endY, 2000);
	//
	//
	//
	//	}
	//
	//	public static void swipeleft(AndroidDriver mDriver, int screenWidth, int screenHeight, int s, int d) {
	//		;
	//
	//		int startY = screenHeight / 2;
	//		int endY = screenHeight / 2;
	//		int startX1 = screenWidth * s / 9;
	//		int endX1 = screenWidth * d / 9;
	//
	//		mDriver.swipe(startX1, startY, endX1, endY, 2000);
	//
	//	}
	//
	public static boolean screenScrol( int q,int p,int r, int t) {
		try{
			if(property.get("platform-name").equals("Android")){
				int screenWidth = ((AndroidDriver)mDriver).manage().window().getSize().getWidth();
				int screenHeight = ((AndroidDriver)mDriver).manage().window().getSize().getHeight();

				int startX = screenWidth *q/ 6;
				int endX = screenWidth *p/ 6;
				int startY = screenHeight* r / 9;
				int endY = screenHeight* t / 9;

				((AndroidDriver)mDriver).swipe(startX, startY, endX, endY, 2000);
				return true;
			}
			else{
				int screenWidth = ((IOSDriver)mDriver).manage().window().getSize().getWidth();
				int screenHeight = ((IOSDriver)mDriver).manage().window().getSize().getHeight();

				int startX = screenWidth *q/ 5;
				int endX = screenWidth *p/ 5;
				int startY = screenHeight* r / 9;
				int endY = screenHeight* t / 9;

				((IOSDriver)mDriver).swipe(startX, startY, endX, endY, 2000);
				return true;
			}      
		}
		catch(Exception e){
			return false;
		}


	}

	//	public static void swipeUP(AndroidDriver mDriver, int screenWidth, int screenHeight, int h, int f) {
	//
	//		int K = 1;
	//		int startX2 = screenWidth / 2;
	//		int endX2 = screenWidth / 2;
	//		int startY2 = screenHeight * h / 9;
	//		int endY2 = screenHeight * f / 9;
	//
	//		mDriver.swipe(startX2, startY2, endX2, endY2, 2000);
	//
	//	}
	//
	//	public static String getText(By locator){
	//
	//		String tValue=null;
	//		wait = new WebDriverWait(((AndroidDriver)mDriver), 20);
	//		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	//
	//		tValue=((AndroidDriver)mDriver).findElement(locator).getText();
	//
	//		if(tValue==null){
	//
	//			System.out.println("Locator doesn't consist of text");
	//
	//		}
	//		else{
	//
	//			return tValue;
	//		}
	//		return tValue;
	//
	//	}
}

package pages;

import org.testng.Assert;
import org.testng.AssertJUnit;
import org.openqa.selenium.By;

import io.appium.java_client.android.AndroidDriver;

public class LoginPage extends BasePage {

	public String fieldName;
	public LoginPage(Object mDriver) {
		super(mDriver);
		// TODO Auto-generated constructor stub
	}
	


	public void clickButton(String field){


		
		 if(field.contains("signIn")){
			fieldName =prop.getProperty("Signin");
		}
		else if(field.contains("home")){
			fieldName =prop.getProperty("Home");
		}
		else if(field.contains("FqaSelect")){
			fieldName =prop.getProperty("fqaSlect");
		}


		bStatus = click("xpath",fieldName,60);

		Assert.assertEquals(bStatus, true, "Element is not Clickable");

	}
	
	public void enterData(String MobileNo,String Password){

		bStatus = click("id", prop.getProperty("MobileNo"), 60);

		Assert.assertEquals(bStatus,true,"Username not clicked");

		bStatus = click("xpath", prop.getProperty("None"),60);
		
		

		Assert.assertEquals(bStatus,true,"None not clicked");

		bStatus = sendText("id", prop.getProperty("MobileNo"), MobileNo, 60);

		Assert.assertEquals(bStatus,true,"Username not Entered");
		
		System.out.println(Password);

		bStatus = sendText("id", prop.getProperty("Password"), Password, 60);

		Assert.assertEquals(bStatus,true,"Password not Entered");
	}
	
	public void clickingLoginNowButton( ){
		bStatus = click("xpath",prop.getProperty("LoginNow"),60);
		if (!bStatus) {
			System.out.println("Element is not clickable");
			AssertJUnit.fail("Element is not clickable");
		}
	}
}


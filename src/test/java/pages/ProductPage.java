package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.AssertJUnit;

import io.appium.java_client.android.AndroidDriver;

public class ProductPage extends BasePage{
	public String   fieldName;
	public ProductPage(Object mDriver) {
		super(mDriver);

	}

	public void clickButton(String field){


		if(field.contains("addToCart")){
			fieldName =prop.getProperty("AddToCart");
		}
		else if(field.contains("goToCart")){
			fieldName =prop.getProperty("GoToCart");
		}

		bStatus = click("xpath",fieldName,60);

		Assert.assertEquals(bStatus, true, "Element is not Clickable");

	}

	
	 public void visibleProduct(String Product){
	 	 
			bStatus = elementVisiblityDynamic("xpath", prop.getProperty("Product"),"Apple iPhone X (Space Gray, 64 GB)", 60);
			if (!bStatus) {
				System.out.println("Element is not clickable");
				Assert.assertEquals(bStatus, true, "Element is not visible");
			}
		
	 }
	 
	 public void checkProduct(String Product){
	 	 
			bStatus = elementVisiblityDynamic("xpath", prop.getProperty("ProductName"),"Apple iPhone X (Space Gray, 64 GB)", 60);
			if (!bStatus) {
				System.out.println("Element is not clickable");
				Assert.assertEquals(bStatus, true, "Element is not visible");
			}
		
	 }
	 
	 public void visibleMyCart(){
	 	 
			bStatus = elementVisiblity("xpath", prop.getProperty("MyCart"), 60);
			if (!bStatus) {
				System.out.println("Element is not clickable");
				Assert.assertEquals(bStatus, true, "Element is not visible");
			}
			
			
		
	 }
	 
	
}

package pages;

import org.openqa.selenium.By;
import org.testng.Assert;

import io.appium.java_client.android.AndroidDriver;



public class SearchPage extends BasePage{ 
	public SearchPage(Object mDriver) {
		super(mDriver);

	}
	public String fieldName;
	public void clickButton(String field){


		if(field.contains("search")){
			fieldName =prop.getProperty("Search");
		}
		else if(field.contains("signIn")){
			fieldName =prop.getProperty("Signin");
		}
		else if(field.contains("home")){
			fieldName =prop.getProperty("Home");
		}
		else if(field.contains("FqaSelect")){
			fieldName =prop.getProperty("fqaSlect");
		}


		bStatus = click("xpath",fieldName,60);

		Assert.assertEquals(bStatus, true, "Element is not Clickable");

	}

	public void enterText(String Product){

		bStatus = sendText("id", prop.getProperty("SearchText"), Product,60);

		Assert.assertEquals(bStatus,true,"Product not entered");

		clickEnter();
	}
	


	public void elementVisiblity(String field){

		if(field.equalsIgnoreCase("recyclerView"))
			fieldName =prop.getProperty("RecyclerView");

		else if(field.contains("ErrorEmail")){
			fieldName =prop.getProperty("EmailErroeweb");
		}
		else if(field.contains("Inavalidcredentials")){
			fieldName =prop.getProperty("Inavlidcredentials");
		}
		else if(field.contains("Accountdetail")){
			fieldName =prop.getProperty("AccountDetails");
		}
	}

}

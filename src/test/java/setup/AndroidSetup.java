package setup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.mysql.cj.jdbc.Driver;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.remote.MobileCapabilityType;

public class AndroidSetup {

	public static Object mDriver;
	public static Properties prop = new Properties();
	public static Properties property = new Properties();
	
		public static DesiredCapabilities capabilities;	


	public static void openApplication(){
		
		try {
			property.load(new FileInputStream(new File(System.getProperty("user.dir")+"\\Resources\\config.properties")));
			System.out.println("openApplication");
			System.out.println(property.get("platform-name"));
			if(property.get("platform-name").equals("Android")){
				prop.load(new FileInputStream(new File(System.getProperty("user.dir")+"\\Resources\\android.properties")));
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability(MobileCapabilityType.APP,"C:\\MockPOC\\Flipkart\\App\\Flipkart_com.flipkart.android.apk");
				capabilities.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.7.2");
				capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "OPPO A57");
				capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "5.1");
				capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
				//capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
				capabilities.setCapability("appActivity", "com.flipkart.android.activity.HomeFragmentHolderActivity");
				capabilities.setCapability("appPackage", "com.flipkart.android"); 
				capabilities.setCapability("autoAcceptAlerts", true);
				capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
				capabilities.setCapability("unicodeKeyboard", true);
				capabilities.setCapability("resetKeyboard", true);

				if (mDriver != null) {
					((AndroidDriver) mDriver).quit();
				}

				mDriver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
				
			}
			else{
				System.out.println("iOS");
				prop.load(new FileInputStream(new File(System.getProperty("user.dir")+"\\Resources\\iOS.properties")));
				System.out.println(prop.getProperty("sample"));
				
			}


		} catch (Exception e) {

			e.printStackTrace();
		}
		//	return (AndroidDriver) mDriver;		

	}
	

}

   Feature: Add a product to cart
   
   Background: 
   	Given user navigate to the login page
    
   Scenario Outline: Search and Add iPhoneX to the cart
   
    When submit valid Email/MobileNo as "<MobileNo>" and password as "<Password>"
		Then user is able to see home page 
		Given user is on Flipkart HomePage
		When user search for product as "<Product>"and Add to cart
		Then product "<Product>" is displayed in Cart page
    
     Examples:
    |MobileNo|Password|Product|
    |9959827791|Planit@123|iPhone X|
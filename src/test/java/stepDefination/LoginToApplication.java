package stepDefination;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeTest;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.HomePage;
import pages.LoginPage;
import pages.ProductPage;
import pages.SearchPage;
import setup.AndroidSetup;


public class LoginToApplication extends AndroidSetup{
	HomePage homePage = new HomePage(mDriver);
	LoginPage loginPage = new LoginPage(mDriver);
	ProductPage productPage = new ProductPage(mDriver);

	
	@Given("^user navigate to the login page$")
	public void user_navigate_to_the_login_page() throws Throwable {
		AndroidSetup.openApplication();
		homePage.clickButton("menu");
		homePage.clickingLinks("myAccount");
	}

	@When("^submit valid Email/MobileNo as \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void submit_valid_username_as_and_password_as(String MobileNo, String Password) throws Throwable {
		loginPage.enterData(MobileNo, Password);
		loginPage.clickButton("signIn");
	}

	@Then("^user is able to see home page$")
	public void user_is_able_to_see_home_page() throws Throwable {
		homePage.clickButton("menu");
		homePage.clickButton("home");
	}

	@Given("^user is on Flipkart HomePage$")
	public void user_is_on_Flipkart_HomePage() throws Throwable {
		homePage.clickingSearch();
	}

	@When("^user search for product as \"([^\"]*)\"and Add to cart$")
	public void user_search_for_product_as_and_Add_to_cart(String Product) throws Throwable {
		homePage.enterText(Product);
		homePage.selectingProduct();
		productPage.visibleProduct(Product);
	}

	@Then("^product \"([^\"]*)\" is displayed in Cart page$")
	public void product_is_displayed_in_Cart_page(String Product) throws Throwable {
		productPage.clickButton("addToCart");
		productPage.clickButton("goToCart");
		productPage.visibleMyCart();
		productPage.checkProduct(Product);
	}

	@After
	public void testShutDown(Scenario scenario) {

		System.out.println(scenario.getName());

		if(scenario.isFailed()) {

			scenario.embed(((TakesScreenshot) mDriver).getScreenshotAs(OutputType.BYTES),"image/png");
		}		
		((RemoteWebDriver) mDriver).quit();
	} 


}
